package adapter;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.davide.androidprojectlayoutv0.R;

import java.util.List;

/**
 * Created by davide on 21/01/17.
 */
public class WifiListAdapter extends ArrayAdapter<ScanResult> {
    private final Context context;
    private final List<ScanResult> values;
    private TextView header;
    private TextView description;

    public WifiListAdapter(Context context, int resource, List<ScanResult> values) {
        super(context, -1, values);
        this.context = context;
        this.values = values;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.info_card, parent, false);

        this.header = (TextView) rowView.findViewById(R.id.card_header);
        this.description = (TextView) rowView.findViewById(R.id.card_description);

        ScanResult result = values.get(position);

        this.header.setText(result.SSID);

        //TODO maybe it's better to show an IP address here.
        this.description.setText("?");

        return rowView;
    }


}
