package view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.davide.androidprojectlayoutv0.R;

/**
 * Created by davide on 17/01/17.
 */
public class RobotInfoCard extends RelativeLayout {
    private TextView header;
    private TextView description;
    private ImageView icon;

    public RobotInfoCard(Context context) {
        super(context);
        init();
    }

    public RobotInfoCard(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RobotInfoCard(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.info_card, this);
        this.header = (TextView) findViewById(R.id.card_header);
        this.description = (TextView) findViewById(R.id.card_description);
        this.icon = (ImageView) findViewById(R.id.card_icon);
    }

    public void setDescription(String s) {
        description.setText(s);
    }
}
