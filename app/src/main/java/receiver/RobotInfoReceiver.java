package receiver;


import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.Random;

/**
 * Created by davide on 17/01/17.
 */
public class RobotInfoReceiver implements Runnable {

    private InformationReceiverListener receiverListener;
    private final Random ran = new Random();

    public RobotInfoReceiver(Context context) {
        Objects.requireNonNull(context);

        if (context instanceof InformationReceiverListener)
            receiverListener = (InformationReceiverListener) context;
    }

    @Override
    public void run() {
        //TODO gather information from robot here

        int x = ran.nextInt(6) + 5;
        if (receiverListener != null) {
            receiverListener.onReceive("TEMP", Integer.toString(x));
        }
    }

    public interface InformationReceiverListener {
        void onReceive(String id, String content);
    }


}
