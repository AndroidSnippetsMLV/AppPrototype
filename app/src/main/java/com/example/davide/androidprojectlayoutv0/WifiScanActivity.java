package com.example.davide.androidprojectlayoutv0;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class WifiScanActivity extends AppCompatActivity {

    private ListView wifiList;

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi_scan);

        wifiList = (ListView) findViewById(R.id.wifiList);
        wifiList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onNetworkSelected(wifiList.getItemAtPosition(position));
            }
        });
        registerForContextMenu(wifiList);


        List<String> l = new ArrayList<>();
        l.add("Network 1");
        l.add("Network 2");
        l.add("Network 3");

        //TODO USE WIFILISTADAPTER HERE!!!!
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, l);

        wifiList.setAdapter(adapter);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_wifi_scan_network_submenu, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_wifi_info:
                Toast.makeText(this, item.getTitle() + " Selected", Toast.LENGTH_SHORT).show();
                return true;
        }
        return true;
    }

    private void confirmNetwork(final String network) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        //Toast.makeText(this, network + " Selected", Toast.LENGTH_SHORT).show();
                        Log.i(WifiScanActivity.class.getName(), "Connecting to \"" + network + "\"");
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Connect to \"" + network + "\"?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    private void onNetworkSelected(Object networkDescription) {
        String network = networkDescription.toString();
        confirmNetwork(network);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_wifi_scan_settings, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_start_wifi_scan: {
                Toast.makeText(this, "Scanning...", Toast.LENGTH_SHORT).show();
                return true;
            }
        }
        return true;
    }
}
