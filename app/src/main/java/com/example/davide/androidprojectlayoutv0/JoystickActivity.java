package com.example.davide.androidprojectlayoutv0;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import receiver.RobotInfoReceiver;
import view.JoystickView;
import view.RobotInfoCard;

import static java.util.concurrent.TimeUnit.SECONDS;

public class JoystickActivity extends AppCompatActivity implements JoystickView.JoystickListener, RobotInfoReceiver.InformationReceiverListener {

    private ScheduledExecutorService scheduler;


    static class ViewHolder {
        public RobotInfoCard tempCard;
    }

    private ViewHolder viewHolder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_joystick);
    }

    private void startGatheringInfo() {
        scheduler = Executors.newScheduledThreadPool(1);
        scheduler.scheduleAtFixedRate(new RobotInfoReceiver(this), 0, 5, SECONDS);
    }

    private void stopGatheringInfo() {
        if (scheduler != null) {
            scheduler.shutdown();
            Toast.makeText(this, "Stopped info gathering", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setContentView(R.layout.activity_joystick);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Log.d(JoystickActivity.class.getName(), "Configuration changed to Landscape");
            setViewHolder();
            startGatheringInfo();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            Log.d(JoystickActivity.class.getName(), "Configuration changed to Portrait");
            stopGatheringInfo();
        }
    }

    void setViewHolder() {
        if (viewHolder != null) {
            return;
        }
        viewHolder = new ViewHolder();
        viewHolder.tempCard = ((RobotInfoCard) findViewById(R.id.temperature_card));
    }

    @Override
    public void onJoystickMoved(float xPercent, float yPercent, int id) {
        Log.d("JoystickActivity", "Joystick Moved by " + xPercent + "," + yPercent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_joystick_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            // TODO implements menu actions logic
        }
        return true;
    }


    @Override
    public void onReceive(String id, final String content) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String currentDateandTime = sdf.format(new Date());
        Log.d(JoystickActivity.class.getName(), "Received [" + id + "] with value [" + content + "] at " + currentDateandTime);

        //Update the widgets' values. Since onReceive is execute on a separate thread, we need to
        //use runOnUiThread method to update the UI's widgets.
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                viewHolder.tempCard.setDescription(content);
            }
        });

        //Set controls value here
    }
}
