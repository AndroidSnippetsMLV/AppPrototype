package com.example.davide.androidprojectlayoutv0;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

import java.util.Objects;

public class RemoteViewActivity extends AppCompatActivity {
    public static final String IP_ADDRESS_ARG = "IP";
    public static final String PORT_ARG = "PORT";
    private static final int DEFAULT_ZOOM_LEVEL = 100;
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Bundle b = getIntent().getExtras();

        String ipAddress = b.getString(IP_ADDRESS_ARG);
        String port = b.getString(PORT_ARG);

        setWebView(ipAddress, port);
    }

    private void setWebView(final String ipAddress, final String port) {
        Objects.requireNonNull(ipAddress, "ip address not valid");
        Objects.requireNonNull(port, "port value not valid");

        webView = (WebView) findViewById(R.id.webView);
        webView.setInitialScale(DEFAULT_ZOOM_LEVEL);
        webView.post(new Runnable() {
            @Override
            public void run() {
                int width = webView.getWidth();
                int height = webView.getHeight();
                //Local IP, Optionally global IP
                webView.loadUrl("http://" + ipAddress + ":" + port + "/stream" + "?width=" + width + "&height=" + height);
            }
        });
    }
}
